package kshrd.com.kh;

import java.util.*;
import java.util.regex.Pattern;
public class EmployeeCtrl {
    private static ArrayList<StaffMember> staffList = new ArrayList();
    public static void message() throws Exception{
        System.out.print("\nPress \"Enter\" key to continue...");
        System.in.read();
    }
    public static void message(String msg) throws Exception{
        System.out.print("》"+msg+": PLEASE INPUT VALID NUMBER!\n");
        System.out.print("\nPress \"Enter\" key to continue...");
        System.in.read();
    }
    public static void message(String msg, String description) throws Exception{
        System.out.print("》"+msg+": "+description+"\n");
        System.out.print("\nPress \"Enter\" key to continue...");
        System.in.read();
    }
    public static void initStaffMember() throws Exception{

        StaffMember volunteer = new Volunteer(1, "Kranh Virackbot","Phnom Penh");
        StaffMember salaries = new EmpSalary(2,"Dy Chiva","Phnom Penh",1230.12,160);
        StaffMember hou = new EmpHourly(3,"Vey Panha","Phnom Penh",150,-2);
        StaffMember volunteer1 = new Volunteer(4, "Bit Sopheap","Phnom Penh");
        StaffMember volunteer2 = new Volunteer(4, "Phon Ratanak","Phnom Penh");


        staffList.add(volunteer);
        staffList.add(salaries);
        staffList.add(hou);
        staffList.add(volunteer1);
        staffList.add(volunteer2);

        showEmployee();
    }


    public static void showEmployee() throws Exception{


        Comparator<StaffMember> staffName = new Comparator<StaffMember>() {
            @Override
            public int compare(StaffMember st1, StaffMember st2) {
                return st1.getName().toUpperCase().compareTo(st2.getName().toUpperCase());
            }
        };
        Collections.sort(staffList, staffName);

        for (Object obj : staffList){
            System.out.println(obj);
        }

        if(staffList.isEmpty()) System.out.println("》EMPLOYEE LIST IS EMPTY!");
    }


    public static void addEmp() throws Exception{
        Scanner sc = new Scanner(System.in);

        String option;
        boolean termOption , event = true;
        while (event){

            System.out.println("1). ✚ Volunteer");
            System.out.println("2). ✚ Salaried Employee");
            System.out.println("3). ✚ Hourly Employee");
            System.out.println("4). ◀ Back");
            System.out.print("\n▶ Please Enter option: ");
            option = sc.next();
            termOption = Pattern.matches("[1-4]",option);

            if (termOption == true){
                switch (option){
                    case "1":

                        StaffMember volunteer = new Volunteer();
                        staffList.add(volunteer);
                        System.out.println("！ VOLUNTEER IS ADDED!");
                        showEmployee();
                        message();
                        break;
                    case "2":

                        StaffMember salaries;
                        try {
                            salaries = new EmpSalary();
                        }catch (Exception ex){
                            message("ERROR","Invalid input!");
                            break;
                        }
                        staffList.add(salaries);
                        System.out.println("！Salaried employee is added!");
                        showEmployee();
                        message();
                        break;
                    case "3":

                        StaffMember hou;
                        try {
                            hou = new EmpHourly();
                        }catch (Exception ex){
                            message("ERROR","Invalid input!");
                            break;
                        }
                        staffList.add(hou);
                        System.out.println("！ Hourly Employee is added!");
                        showEmployee();
                        message();
                        break;
                    case "4":
                        event = false;
                        break;
                }
            }else {
                message("MESSAGE");
            }
        }
    }


    public static void EditEmp() throws Exception{
        Scanner sc = new Scanner(System.in);

        System.out.print("◈ Input Id\t\t: ");
        int id,index = -1;
        Object getObj = null;
        boolean isEmpty = true;
        try {
            id = sc.nextInt();
            Iterator<StaffMember> temp = staffList.iterator();
            while (temp.hasNext()){
                Object obj = temp.next();

                if (((StaffMember) obj).getId() == id){
                    index = staffList.indexOf(obj);
                    System.out.println(obj);
                    getObj = ((StaffMember) obj).modifyObject(id);
                    isEmpty = false;
                }
            }

            if (isEmpty){
                message("MESSAGE","Employee is not FOUND!");
            }else {
                staffList.set(index,(StaffMember) getObj);
                System.out.println("！ UPDATE SUCCESSFUL!");
                showEmployee();
                message();
            }
        }catch (Exception ex){
            message("ERROR","INVALID INPUT!");
        }
    }

    public static void removeEmp() throws Exception{
        Scanner sc = new Scanner(System.in);
        System.out.print("◈ Input Id\t\t: ");
        int id;
        boolean isEmpty = true;
        try {
            id = sc.nextInt();
            Iterator<StaffMember> temp = staffList.iterator();
            while (temp.hasNext()){
                Object obj = temp.next();
                if (((StaffMember) obj).getId() == id){
                    System.out.println(obj);
                    isEmpty = false;
                    System.out.print("！ DO YOU WANT TO DELETE THIS EMPLOYEE? (Y/N): ");
                    String confirm = sc.next().toUpperCase();
                    switch (confirm){
                        case "Y":
                            temp.remove();
                            message("MESSAGE","EMPLOYEE IS DELETED SUCCESSFUL!");
                            break;
                        default:
                            message("MESSAGE","ACTION IS CANCELED!");
                            break;
                    }
                }
            }

            if (isEmpty){
                message("MESSAGE","Not Found !");
            }else {
                showEmployee();
                message();
            }
        }catch (Exception ex){
            message("ERROR","Invalid input!");
        }
    }


}
