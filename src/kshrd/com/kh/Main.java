package kshrd.com.kh;

import java.util.Scanner;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);

        EmployeeCtrl.initStaffMember();

        String option;
        boolean termOption;
        while (true){
            System.out.println("1). ✚ Add Employee");
            System.out.println("2). ✎ Edit");
            System.out.println("3). ✘ Remove ");
            System.out.println("4). ☐ Exit");
            System.out.print("\n▶ PLEASE INPUT OPTION: ");
            option = sc.next();

            termOption = Pattern.matches("[1-4]",option);

            if (termOption == true){
                switch (option){
                    case "1":

                        EmployeeCtrl.addEmp();
                        break;
                    case "2":

                        EmployeeCtrl.EditEmp();
                        break;
                    case "3":

                        EmployeeCtrl.removeEmp();
                        break;
                    case "4":

                        System.out.print("EXIT!");

                        System.exit(0);
                        break;
                }
            }else {

                EmployeeCtrl.message("MESSAGE");
            }
        }
    }
}