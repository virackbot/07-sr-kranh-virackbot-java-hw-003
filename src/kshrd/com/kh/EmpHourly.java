package kshrd.com.kh;

import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.Table;
import java.util.Scanner;

public class EmpHourly extends StaffMember {
    Scanner sc = new Scanner(System.in);

    private int hoursWorked;
    private double rate;


    @Override
    public double pay() {
        return getHoursWorked() >= 0 && getRate() >= 0 ? getHoursWorked() * getRate() : -1;
    }


    @Override
    public Object modifyObject(int id) {

        EmpHourly hour = new EmpHourly(id);
        return hour;
    }

    public EmpHourly(){
        System.out.print("◈ HoursWorked\t: ");
        this.hoursWorked = sc.nextInt();
        System.out.print("◈ Rate\t\t\t: ");
        this.rate = sc.nextDouble();
    }


    public EmpHourly(int id) {
        super(id);
        System.out.print("◈ Input New HoursWorked\t: ");
        this.hoursWorked = sc.nextInt();
        System.out.print("◈ Input New Rate\t\t: ");
        this.rate = sc.nextDouble();
    }


    public EmpHourly(int id, String name, String address, int hoursWorked, double rate) {
        super(id, name, address);
        this.hoursWorked = hoursWorked;
        this.rate = rate;
    }


    @Override
    public String toString() {
        Table table = new Table(2, BorderStyle.UNICODE_BOX_DOUBLE_BORDER);
        table.setColumnWidth(1, 20, 25);
        table.setColumnWidth(0, 20, 25);
        table.addCell("HOURLY EMPLOYEE", new CellStyle(CellStyle.HorizontalAlign.center),2);
        table.addCell("ID");
        table.addCell(id+"");
        table.addCell("Name");
        table.addCell(name+"");
        table.addCell("Address");
        table.addCell(address+"");
        table.addCell("Hours Worked");
        table.addCell(hoursWorked+"");
        table.addCell("Rate");
        table.addCell(rate+"");
        table.addCell("Payment");
        table.addCell(pay() == -1 ? "null" : String.format("%.2f",pay()));
        return  table.render();
    }


    public int getHoursWorked() {
        return hoursWorked;
    }

    public void setHoursWorked(int hoursWorked) {
        this.hoursWorked = hoursWorked;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }
}

