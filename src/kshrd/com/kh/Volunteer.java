package kshrd.com.kh;

import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.Table;

public class Volunteer extends StaffMember{
    @Override
    public double pay() {
        return 0;
    }
    @Override
    public Object modifyObject(int id) {
        Volunteer volunteer = new Volunteer(id);
        return volunteer;
    }


    public Volunteer() {
    }
    public Volunteer(int id) {
        super(id);
    }

    public Volunteer(int id, String name, String address) {
        super(id, name, address);
    }


    @Override
    public String toString() {
        Table table = new Table(2, BorderStyle.UNICODE_BOX_DOUBLE_BORDER);
        table.setColumnWidth(1, 20, 25);
        table.setColumnWidth(0, 20, 25);

        table.addCell("VOLUNTEER", new CellStyle(CellStyle.HorizontalAlign.center),2);
        table.addCell("ID");
        table.addCell(id+"");
        table.addCell("Name");
        table.addCell(name +"");
        table.addCell("Address");
        table.addCell(address+"");
        table.addCell("Thanks", new CellStyle(CellStyle.HorizontalAlign.center),2);
        return  table.render();
    }
}